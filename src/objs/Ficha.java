package objs;

public class Ficha {
	private char character;
	private boolean vertical;
	private boolean horizontal;
	
	
	public Ficha(char character, boolean horizontal, boolean vertical){
		this.character = character;
		this.vertical = vertical;
		this.horizontal = horizontal;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ficha other = (Ficha) obj;
		return (other.character==character);
	}
		

	public char getCharacter() {
		return character;
	}

	@Override
	public String toString(){
		return (""+character);
	}

	public boolean getVertical() {
		return vertical;
	}


	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}


	public boolean getHorizontal() {
		return horizontal;
	}


	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}
	
	
}
