package objs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Board {
	private static final int OUT_OF_BOUNDS = 45;
	private static final char LETTERS_SIZE = 'a';
	private static final int COL = 15;
	private static final int ROW = 15;
	private Ficha[][] board = new Ficha[ROW][COL];
	private int puntaje;
	private int[] cantLetras = new int[26];
	private int offset;
	private int sumLettersLeft;
	
	
	public Board(){
		return;
	}
	
	public int getPuntaje(){
		return puntaje;
	}
	
	public void addScore(int score){
		puntaje = puntaje + score;
	}
	public int[] getCantLetras(){
		return cantLetras;
	}
	
	public void setOffset(int offSet){
		this.offset=offSet;
	}
	
	public int getOffSet(){
		return offset;
	}
	
	public Ficha get(Position position){
		if( (position.getCol()>=ROW || position.getCol()<0) || (position.getRow()>=COL || position.getRow()<0) ) {//KAKA
			return new Ficha((char)OUT_OF_BOUNDS,false,false); 
		}
		if(board[position.getRow()][position.getCol()]==null){
			return new Ficha((char)0,false,false); 
		}
		return board[position.getRow()][position.getCol()];
	}

	@Override
	public int hashCode() {
		String palabra = "";
		for(int i=0; i < ROW; i++){
			for(int j=0; j < COL; j++){
				if(this.board[i][j] != null){
					palabra += this.board[i][j].getCharacter();
				}
			}
		}
		return palabra.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		for(int i = 0; i < ROW; i++){
			for(int j=0; j < COL; j++){
				if(this.board[i][j]==null){
					if(other.board[i][j]!=null) return false;
				}else{
					if( !(this.board[i][j].equals((other.board[i][j])))){
						return false;
					}
				}
				
			}
		}
		
		return true;
	}
	
	public int getSumLettersLeft(){
		return sumLettersLeft;
	}

	public int getCantCharacter(char character) {
		if(cantLetras==null) throw new IllegalStateException("Se le tiene que pasar las letras al board");
		if(character-LETTERS_SIZE>=26 || character-LETTERS_SIZE<0) throw new IllegalStateException("character no deberia poder ser tan grande/chico");
		return cantLetras[character-LETTERS_SIZE];
	}

	public void decreaseCharacter(char character) {
		if(cantLetras[character-LETTERS_SIZE]<=0) throw new IllegalStateException("No deberia poner una letra que no tengo");
		cantLetras[character-LETTERS_SIZE]-=1;
		sumLettersLeft-=1;
	}

	public void putChar(Position position, char charAt, int[] direction) {
		
		if(get(position).getCharacter()!=0){
			throw new IllegalArgumentException();
		}
		if(!(position.getCol() < COL && position.getCol()>=0 && position.getRow() < ROW && position.getRow()>=0)){
			throw new IllegalArgumentException("No deberia poner char afuera del tablero");
		}
		board[position.getRow()][position.getCol()]=new Ficha(charAt,direction[1]==1,direction[0]==1);
		
	}

	public Board copy() {
		Board retBoard = new Board();
		retBoard.board = new Ficha[15][15];
		for(int i=0; i<ROW;i++){
			for(int j=0; j<ROW;j++){
				if(board[i][j]!=null) retBoard.board[i][j]=new Ficha(board[i][j].getCharacter(),board[i][j].getHorizontal(),board[i][j].getVertical());
			}
		}
		retBoard.cantLetras = cantLetras.clone();
		retBoard.puntaje = puntaje;
		retBoard.sumLettersLeft = sumLettersLeft;
		retBoard.offset = offset;
		return retBoard;
	}

	public void setCantLetras(int[] cantLetras) {
		this.cantLetras=cantLetras.clone();
		sumLettersLeft=0;
		for(int i: cantLetras){
			sumLettersLeft+=i;
		}
	}
	
	
	public void print(){
		
		for(int i=0; i<ROW; i++){
			for(int j=0; j<COL;j++){
				if(board[i][j]==null) System.out.print("  ");
				else{
					System.out.print(board[i][j]);
					System.out.print(" ");
					
				}
			}
			System.out.println();
		}
		System.out.println("Puntajes: "+puntaje);
	}

	public void shift(int offSetNeeded) {
		for(int j=0; j<COL-offSetNeeded; j++){
			for(int i=0; i<ROW; i++){
				board[i][j]=board[i][j+offSetNeeded];
			}
		}
		for(int j=COL-offSetNeeded; j<COL; j++){
			for(int i=0; i<ROW; i++){
				board[i][j]=null;
			}
		}
		offset-=offSetNeeded;
		
	}
	
	public String toString(){
		String ret = "";
		for(int i=0; i<ROW; i++){
			for(int j=0; j<COL;j++){
				if(board[i][j]==null) ret+=" ";
				else{
					ret+=String.valueOf(board[i][j].getCharacter());
				}
				ret+=" ";
			}
			ret+="\n";
			
		}
		
		ret+="Puntajes: "+puntaje;
		ret+="\n";
		return ret;
	}
	
	
}
