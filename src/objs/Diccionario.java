package objs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;



public class Diccionario{
	private HashMap<String,Node> first;
	private int lengthAverage = 0;
	private static final int MAYUSCULA = 'A'-'a';

	
	
		
	public Diccionario(){
		this.first = new HashMap<String,Node>();
	}
	
	public void addWord(String s){
		HashMap<String,Node> current=first;
		for(int i=0; i<s.length();i++){
			if(s.charAt(i) <= 'z' || s.charAt(i) <= 'a'){
				if(!current.containsKey(""+s.charAt(i))){
					current.put(""+(s.charAt(i)),new Node());
				}
				if(i==s.length()-1){
					current.get(""+(s.charAt(i))).setIsWord(true);
				}
				current=current.get(""+(s.charAt(i))).getNextNodes();
			}
			
		}
	
		
	}
	
	public HashMap getFirst(){
		return this.first;
	}
	
	public void fileToDiccionario(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	    	boolean flag = true;
	        String line = br.readLine();
	        int count = 0;
	        while (line != null) {
	        	int largo = line.length();
	        	if(largo >= 2 && largo <= 7){
	        		for(int i = 0; i<largo;i++){
	        			if(!((line.charAt(i)>='a' && line.charAt(i)<='z') || (line.charAt(i)>='A' && line.charAt(i)>='B'))){
	        				flag = false;
	        			}
	        		}
	        		if(flag){
	        			addWord(line.toLowerCase());
		 	            lengthAverage += line.length();
		 	            line = br.readLine();
		 	            count++;
	        		}
	        		
	        	}else{
	        		line = br.readLine();
	        	}
	        }
	        lengthAverage = lengthAverage/count;
	    } finally {
	        br.close();
	    }
	}

	public int getLengthAverage() {
		return lengthAverage;
		
	}
	

	
	

}
