package objs;

import java.util.HashMap;




public class Node{
	private HashMap<String,Node> nextNodes;
	private boolean isWord;
	
	public Node(){
		this.nextNodes = new HashMap();
		isWord=false;
	}
	
	public HashMap getNextNodes(){
		return this.nextNodes;
	}
	
	public void setIsWord(boolean value){
		this.isWord = value;
	}
	
	public boolean getIsWord(){
		return this.isWord;
	}
}

