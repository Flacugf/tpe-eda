package objs;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class main {

	
	public static void main(String[] args) throws IOException {
		Diccionario diccionario = new Diccionario();
		int[] puntajes = {1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10};
		Board bestBoard = new Board();
		Scrabble scrabble = new Scrabble(diccionario, puntajes);
		
		
		diccionario.fileToDiccionario(args[0] + ".txt");
		scrabble.fileToCantLetras(args[1] + ".txt");
		if(args.length >= 4 && args[3].equals("-visual")){
			if(args.length == 4) bestBoard = scrabble.getBestSolution(true);
			else bestBoard = scrabble.getAproxSolution(Double.parseDouble(args[5]), true);
		}else{
			if(args.length == 3) bestBoard = scrabble.getBestSolution(false);
			else bestBoard = scrabble.getAproxSolution(Double.parseDouble(args[4]),false);
		}
		bestBoard.print();
	      BufferedWriter writer = null;
	        try {
	            String boardString = bestBoard.toString();
	            File boardFile = new File(args[2] + ".txt");
	            writer = new BufferedWriter(new FileWriter(boardFile));
	            writer.write(boardString);
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                writer.close();
	            } catch (Exception e) {
	            }
	        }
	     
		
	}
}
