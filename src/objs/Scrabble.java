package objs;
import java.util.Random;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class Scrabble {
	private static final int OUT_OF_BOUNDS = 45;
	private static final char LETTERS_SIZE = 'a';
	private static final int MAYUSCULA = 'A'-'a';
	private static final int CONST = 'a';
	private static final int COL = 15;
	private static final int ROW = 15;
	//private Board board;
	private int[] puntajes;
	private HashSet<Board> states;
	private Diccionario diccionario;
	private int[]letras;
	
	
	
	public Scrabble(Diccionario diccionario, int[]puntajes){
		this.diccionario = diccionario;
		states = new HashSet();
		this.puntajes = puntajes;
		this.letras=new int[26];
	}
	
	
	
	
	/*
	 * Devuelve el mejor tablero que se pueda hacer con ese diccionario y esas letras.
	 */
	public Board getBestSolution(boolean visual){
		Board board = new Board();	
		board.setCantLetras(letras);
		
		Board bestBoard = getNextWord(new Position(ROW/2,COL/2),board, diccionario.getFirst(),"",board.getCantLetras().clone(), visual);
		return bestBoard;
	}
	
	
	
	
	
	/*
	 * Devuelve el mejor tablero que se pueda hacer con ese diccionario, esas letras y una position. A partir de un tablero.
	 */
	private Board getNextWord(Position position,Board board, HashMap<String,Node> lettersMap, String word, int[] cantLettrasCopy, boolean visual) { 
		Board bestBoard = board;
		for(String s: lettersMap.keySet()){
			String thisWord = word+s;
			if(lettersMap.get(s).getNextNodes()!=null){
				if(cantLettrasCopy[s.charAt(0)-CONST]>0){
					cantLettrasCopy[s.charAt(0)-CONST]-=1;
					Board nextLevelBoard=getNextWord(position, board, lettersMap.get(s).getNextNodes(), thisWord, cantLettrasCopy, visual);
					cantLettrasCopy[s.charAt(0)-CONST]+=1;
					if(nextLevelBoard.getPuntaje()>bestBoard.getPuntaje()){
						bestBoard=nextLevelBoard;
					}
					if(bestBoard.getSumLettersLeft()==0) return bestBoard;
					
				}				
			}
			if(lettersMap.get(s).getIsWord()){
				Board auxBoard=board; 
				auxBoard=putWord(position, thisWord, board, visual);
				if(auxBoard.getPuntaje()>bestBoard.getPuntaje()){
					bestBoard=auxBoard;
				}
				if(bestBoard.getSumLettersLeft()==0) return bestBoard;
				
			}
		}
		return bestBoard;
	}


	/*
	 * Va por cada letra del tablero y prueba en las letras en donde se pueda(no tengan ambos booleanos) si 
	 * puede poner una palabra en esa direccion.
	 */
	private Board getBestSolutionRecursive(Board board, boolean visual){
		Board bestBoard = board;
		Board auxBoard = board;
		for(int row = 0; row < ROW;row++){
			for(int col=0; col < COL;col++){
				Position auxPosition = new Position(row,col);
				if(board.get(auxPosition).getCharacter()!=0){
					if(!board.get(auxPosition).getHorizontal()||!board.get(auxPosition).getVertical()){

						int[] direction = getDirection(board,auxPosition);
						Position auxPositionLetters; 
						int[] newLetters = board.getCantLetras().clone();
						for(int i=0; i<COL; i++){ //COL == ROW. 
							auxPositionLetters = new Position(row*direction[1]+i*direction[0],col*direction[1]+i*direction[0]);
							if(board.get(auxPosition).getCharacter()!=0){
								newLetters[board.get(auxPosition).getCharacter()-CONST]++;
							}
						}
						auxBoard=getNextWord(auxPosition, board, diccionario.getFirst(),"",newLetters, visual);
						if(board.get(auxPosition).getHorizontal()){
							board.get(auxPosition).setVertical(true);
						}else{
							board.get(auxPosition).setHorizontal(true);
						}
						if(auxBoard.getPuntaje()>bestBoard.getPuntaje()) {
							bestBoard = auxBoard;
						}
						if(bestBoard.getSumLettersLeft()==0) return bestBoard;
					}
				}
			}
		}
		return bestBoard;
	}
	
	
	/*
	 * Devuelve la mejor solucion que encuentra en el tiempo que tenga.
	 */
	public Board getAproxSolution(double time, boolean visual){
		double startingTime = System.currentTimeMillis()/1000.0;
		Board bestBoard = null;
		while((System.currentTimeMillis()/1000) - startingTime < time){
			Board auxBoard = new Board();
			auxBoard.setCantLetras(letras);
			auxBoard = getNextWordAprox(new Position(ROW/2,COL/2),auxBoard, diccionario.getFirst(),"", startingTime, time, auxBoard.getCantLetras().clone(), visual);
			if(bestBoard == null || auxBoard.getPuntaje()>bestBoard.getPuntaje()){
				bestBoard = auxBoard;
			}
			if(bestBoard.getSumLettersLeft()==0 || (System.currentTimeMillis()/1000.0)-startingTime > time) return bestBoard;
			
		}
		return bestBoard;
	}
	
	
	private Board getNextWordAprox(Position position,Board board, HashMap<String,Node> lettersMap, String word, double startingTime, double endingTime, int[] cantLettrasCopy, boolean visual) {
		Board bestBoard = board;
		Random randomInt = new Random();
		int length = lettersMap.keySet().toArray().length;
		int random = Math.abs(randomInt.nextInt(25));
		for(int i = 0; length > i; i++){
			String s = (String) lettersMap.keySet().toArray()[(i+random)%length];
			String thisWord = word + s;
			if(lettersMap.get(s).getIsWord() && isWordProbability(s)){
				Board auxBoard=board; 
				auxBoard=putWordAprox(position, thisWord, board,startingTime,endingTime, visual);
				if(auxBoard.getPuntaje()>bestBoard.getPuntaje()){
					bestBoard=auxBoard;
				}
				if(bestBoard.getSumLettersLeft()==0 || (System.currentTimeMillis()/1000.0)-startingTime > endingTime) return bestBoard;
			}
			if(lettersMap.get(s).getNextNodes()!=null){
				if(cantLettrasCopy[s.charAt(0)-CONST]>0){
					cantLettrasCopy[s.charAt(0)-CONST]-=1;
					Board nextLevelBoard=getNextWordAprox(position, board, lettersMap.get(s).getNextNodes(), thisWord,  startingTime, endingTime, cantLettrasCopy, visual);
					cantLettrasCopy[s.charAt(0)-CONST]+=1;
					if(nextLevelBoard.getPuntaje()>bestBoard.getPuntaje()){
						bestBoard=nextLevelBoard;
					}
					if(bestBoard.getSumLettersLeft()==0 || (System.currentTimeMillis()/1000.0)-startingTime > endingTime) return bestBoard;
				}
			}	
		}
		return bestBoard;
	}
	
	public boolean isWordProbability(String s){
		int lengthAverage = diccionario.getLengthAverage();
		if(s.length() > lengthAverage){
			if(getProbability(80)){
				return true;
			}
				return false;
		}else{
			if(getProbability(40)){
				return true;
			}else{
				return false;
			}
		}
	}

	




	private boolean getProbability(int i) {
		Random random = new Random();
			if(random.nextInt(100) <= i){
				return true;
		}else{
			return false;
		}
	}




	/*
	 * Verificar si la palabra puede ir, retornar un nuevo tablero con la palabra
	 * si es valido y null si no es valido. Retorna el mejor tablero posible.
	 */
	private Board putWord(Position position, String thisWord, Board board, boolean visual) {
		if(board.getSumLettersLeft()==0) return board;
		char letra = board.get(position).getCharacter();
		Board newBoard=board;
		Board bestBoard=board;
		if(letra==0){
			newBoard = board.copy();
			newBoard.setOffset(thisWord.length()-1);
			if(!literallyPutWord(position,0,thisWord,newBoard)){
				return board;
			}
			if(visual){
				newBoard.print();
			}
			bestBoard=	getBestSolutionRecursive(newBoard, visual);	
			if(bestBoard.getSumLettersLeft()==0) {
				return bestBoard;
			}
			
		}else{
			bestBoard=board;
			for(int i=0; i<thisWord.length(); i++){
				boolean esValido = false;
				int[] direction = getDirection(board,position);
				if(thisWord.charAt(i)==letra &&  position.getRow()-i*direction[0]>=0 && position.getCol()-i*direction[1]>=0 &&  position.getRow()+(-i+thisWord.length()-1)*direction[0]<ROW ){ //&&  position.getCol()+(-i+thisWord.length()-1)*direction[1]<COL 
					esValido = true;
					
					if(!((board.get(new Position(position.getRow()-(i+1)*direction[0],position.getCol()-(i+1)*direction[1])).getCharacter()==OUT_OF_BOUNDS || board.get(new Position(position.getRow()-(i+1)*direction[0],position.getCol()-(i+1)*direction[1])).getCharacter()==0) && (board.get(new Position(position.getRow()+(-i+thisWord.length())*direction[0],position.getCol()+(-i+thisWord.length())*direction[1])).getCharacter()==OUT_OF_BOUNDS
							|| board.get(new Position(position.getRow()+(-i+thisWord.length())*direction[0],position.getCol()+(-i+thisWord.length())*direction[1])).getCharacter()==0))){
							esValido=false; 
						
					}
					
					for(int j = 0; j<thisWord.length() && esValido; j++){
						if(j!=i){
							esValido = validCharacter(position,i,j,thisWord.charAt(j), board);
						}
					}
					
				} 
				if(esValido){
					newBoard = board.copy();
					if(!literallyPutWord(position,i,thisWord,newBoard)){
						newBoard=board;
					}else{
						if(visual){
							newBoard.print();
						}
							Board auxBoard = getBestSolutionRecursive(newBoard, visual);
							if(auxBoard.getPuntaje()>newBoard.getPuntaje()){
								newBoard = auxBoard;
							}
							if(bestBoard.getSumLettersLeft()==0) return bestBoard;
							
						
					}
				}
				if(newBoard.getPuntaje()>bestBoard.getPuntaje()){
					bestBoard=newBoard;
				}
			}	
		}
		return bestBoard;
	}
	
	private int[] getDirection(Board board, Position position) {
		final int[] HORIZONTAL={0,1};
		final int[] VERTICAL={1,0};
		int[] direction;
		if(!board.get(position).getHorizontal()){
			direction=HORIZONTAL;
		}else{
			if(board.get(position).getVertical()){
				throw new IllegalStateException("No hay que tratar de poner palabras si se tienen ambos flag prendidos");
			}
			direction=VERTICAL;
		}
		return direction;
	}




	private boolean literallyPutWord(Position position, int i, String thisWord,Board board) {
		int[] direction = getDirection(board, position);
		
			
		Position auxPosition = new Position(position.getRow()-(i)*direction[0],position.getCol()-(i)*direction[1]);
		for(int j=0;j<thisWord.length();j++){
			char character = board.get(auxPosition).getCharacter();
			if(character==OUT_OF_BOUNDS){
				int offSetNeeded = thisWord.length()-j;
				if(offSetNeeded > board.getOffSet()){
					return false;
				}else{
					board.shift(offSetNeeded);
				}
				auxPosition = new Position(auxPosition.getRow()-(offSetNeeded)*direction[0],auxPosition.getCol()-(offSetNeeded)*direction[1]);
				character = board.get(auxPosition).getCharacter();
			}
			if(character==0){//Esta vacio
				char letraAPoner = thisWord.charAt(j);
				
				if(board.getCantCharacter(letraAPoner) == 0){ //el caracter que quiero poner
					return false;
				}if(auxPosition.getRow() < ROW && auxPosition.getRow() >= 0 && auxPosition.getCol() < COL && auxPosition.getCol() >= 0){
					board.decreaseCharacter(letraAPoner);
					board.putChar(auxPosition,thisWord.charAt(j),direction);
					board.setOffset(Math.min(board.getOffSet(), auxPosition.getCol()));
					board.addScore(puntajes[letraAPoner-CONST]);
				}else{
					return false;
				}
				
			}
			
			board.get(auxPosition).setHorizontal(board.get(auxPosition).getHorizontal() || direction[1]==1);
			board.get(auxPosition).setVertical(direction[0]==1||board.get(auxPosition).getVertical());
			auxPosition = new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1]);
			
		}
		
		return true;
	}


	private boolean inLineCheckWord(Position position, Board board, char charAt, int[] direction) {
			
		
		
		Position auxPosition = position;
		if(board.get(new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1])).getCharacter()==0 && board.get(new Position(auxPosition.getRow()-direction[0],auxPosition.getCol()-direction[1])).getCharacter()==0 ){
			return true;
		}
		int count=0;
		if(auxPosition.getCol()>=0 && auxPosition.getRow()>=0){
			auxPosition = new Position(auxPosition.getRow()-direction[0],auxPosition.getCol()-direction[1]);
		}
		while(auxPosition.getCol()>=0 && auxPosition.getRow()>=0 && board.get(auxPosition).getCharacter()!=0){
			auxPosition = new Position(auxPosition.getRow()-direction[0],auxPosition.getCol()-direction[1]); 
			count+=1;
		}
		if(board.get(auxPosition).getCharacter()==0){
			auxPosition = new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1]); 
		}
		boolean isWord = false;
		
		HashMap<String,Node> letterMap = diccionario.getFirst();
		
		

		while(count > 0){
			char charAtAux = board.get(auxPosition).getCharacter();
			if(!letterMap.containsKey(""+(charAtAux))){
				return false;
			}
			if( (board.get(auxPosition).getHorizontal() && direction[1]==1) || (board.get(auxPosition).getVertical() && direction[0]==1) ){
				return false; 
			}
			isWord = (letterMap.get(""+(charAtAux))).getIsWord();
			letterMap=letterMap.get(""+(charAtAux)).getNextNodes();
			auxPosition = new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1]);
			count-=1;
		}
		if(!letterMap.containsKey(""+(charAt))){
			return false;
		}else{
			isWord = (letterMap.get(""+(charAt))).getIsWord();
			letterMap=letterMap.get(""+(charAt)).getNextNodes();
			auxPosition = new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1]);
		}
		
		while(board.get(auxPosition).getCharacter() != OUT_OF_BOUNDS && board.get(auxPosition).getCharacter() != 0){
			
			char charAtAux = board.get(auxPosition).getCharacter();
			if(!letterMap.containsKey(""+(charAtAux))){
				return false;
			}
			if( (board.get(auxPosition).getHorizontal() && direction[1]==1) || (board.get(auxPosition).getVertical() && direction[0]==1) ){
				return false;
			}
			isWord = (letterMap.get(""+(charAtAux))).getIsWord();
			letterMap=letterMap.get(""+(charAtAux)).getNextNodes();
			auxPosition = new Position(auxPosition.getRow()+direction[0],auxPosition.getCol()+direction[1]);
			
		}
		return isWord;
		
	}



		private boolean validCharacter(Position position, int i, int j, char charAt, Board board) {
			int[] direction = getDirection(board, position);
			Position auxPosition = new Position(position.getRow()+(-i+j)*direction[0],position.getCol()+(-i+j)*direction[1]);
			if(board.get(auxPosition).getCharacter()==OUT_OF_BOUNDS){
				return true;
			}
			if(board.get(auxPosition).getCharacter()!=0){
				return board.get(auxPosition).getCharacter()==charAt;
			}else{
				int aux = direction[0];
				direction[0]=direction[1];
				direction[1]=aux;
				return inLineCheckWord(auxPosition, board, charAt, direction);
			}
		}



		
		
	
		public void fileToCantLetras(String fileName) throws IOException {
		    BufferedReader br = new BufferedReader(new FileReader(fileName));
		    try {
		        String line = br.readLine();
		        String text = "";
		        while (line != null) {
		            text += line;
		            line = br.readLine();
		        }
		        int i = 0;
		        int length = text.length();
		        while(i < length && i <= 80){
		        	char letra = text.charAt(i);
		        	if((letra <= 'z' && letra >= 'a')){
		        		letras[text.charAt(i)-LETTERS_SIZE]++;
			        	
		        	}
	        		if(letra <= 'Z' && letra >= 'A'){
	        			letras[letra-LETTERS_SIZE-MAYUSCULA]++;
	        			
	        		}
		        	i++;
		        	
		        }
		    } finally {
		        br.close();
		    }
		}
		
		private Board putWordAprox(Position position, String thisWord, Board board, double startingTime, double endingTime, boolean visual) {
			char letra = board.get(position).getCharacter();
			Board newBoard=board;
			Board bestBoard=board;
			if(letra==0){
				newBoard = board.copy();
				newBoard.setOffset(0);
				if(!literallyPutWord(position,thisWord.length()/2,thisWord,newBoard)){
					return board;
				}
				if(visual){
					newBoard.print();
				}
				bestBoard=getBestSolutionRecursiveAprox(newBoard, startingTime, endingTime, visual);
				if(bestBoard.getSumLettersLeft()==0 || (System.currentTimeMillis()/1000.0)-startingTime > endingTime) return bestBoard;
			}else{
				
				
				bestBoard=board;
				for(int i=0; i<thisWord.length(); i++){
					boolean esValido = false;
					int[] direction = getDirection(board,position);
					if(thisWord.charAt(i)==letra &&  position.getRow()-i*direction[0]>=0 && position.getCol()-i*direction[1]>=0 &&  position.getRow()+(-i+thisWord.length()-1)*direction[0]<ROW &&  position.getCol()+(-i+thisWord.length()-1)*direction[1]<COL ){ 
						esValido = true;
						if(board.get(new Position(position.getRow()-(i+1)*direction[0],position.getCol()-(i+1)*direction[1])).getCharacter()!=OUT_OF_BOUNDS || board.get(new Position(position.getRow()+(-i+thisWord.length())*direction[0],position.getCol()+(-i+thisWord.length())*direction[1])).getCharacter()!=OUT_OF_BOUNDS){
							if(board.get(new Position(position.getRow()-(i+1)*direction[0],position.getCol()-(i+1)*direction[1])).getCharacter()!=0 || board.get(new Position(position.getRow()+(-i+thisWord.length())*direction[0],position.getCol()+(-i+thisWord.length())*direction[1])).getCharacter()!=0){
								esValido=false; 
							}
						}
						for(int j = 0; j<thisWord.length() && esValido; j++){
							if(j!=i){
								esValido = validCharacter(position,i,j,thisWord.charAt(j), board);
							}
						}
												
					}
					if(esValido){
						newBoard = board.copy();
						if(!literallyPutWord(position,i,thisWord,newBoard)){
							newBoard=board;
						}else{
							if(visual){
								newBoard.print();
							}
							return getBestSolutionRecursiveAprox(newBoard, startingTime, endingTime, visual); 
						}
					}
				}	
			}
			return bestBoard;
		}




		private Board getBestSolutionRecursiveAprox(Board board, double startingTime, double endingTime, boolean visual) {
			Board bestBoard = board;
			Board auxBoard = board;
			Random random = new Random();
			int randomRow = random.nextInt(ROW);
			int randomCol = random.nextInt(COL);
			for(int row = 0; row < ROW;row++){
				for(int col=0; col < COL;col++){
					Position auxPosition = new Position((row+randomRow)%ROW,(col+randomCol)%COL);
					if(board.get(auxPosition).getCharacter()!=0){
						if((!board.get(auxPosition).getHorizontal()||!board.get(auxPosition).getVertical()) && useLetterProbability()){
							int[] direction = getDirection(board,auxPosition);
							Position auxPositionLetters; 
							int[] newLetters = board.getCantLetras().clone();
							for(int i=0; i<COL; i++){ //COL == ROW. 
								auxPositionLetters = new Position(row*direction[1]+i*direction[0],col*direction[1]+i*direction[0]);
								if(board.get(auxPositionLetters).getCharacter()!=0){
									newLetters[board.get(auxPosition).getCharacter()-CONST]++;
								}
							}
							auxBoard=getNextWordAprox(auxPosition, board, diccionario.getFirst(),"", startingTime,endingTime,newLetters, visual);
							if(board.get(auxPosition).getHorizontal()){
								board.get(auxPosition).setVertical(true);
							}else{
								board.get(auxPosition).setHorizontal(true);
							}
							if(auxBoard.getPuntaje()>bestBoard.getPuntaje()) {
								bestBoard = auxBoard;
							}
							if(bestBoard.getSumLettersLeft()==0 || (System.currentTimeMillis()/1000.0)-startingTime > endingTime) return bestBoard;
						}
					}
				}
			}
			return bestBoard;
		}




		private boolean useLetterProbability() {
			return getProbability(70);
		}
		
		
	
}
